﻿using SourceQuery;

ReadIP:
Console.Write("Server Address: ");
var address = Console.ReadLine();
if (address == null) goto ReadIP;

var port = 27015;

if (address.Contains(':'))
{
    var splt = address.Split(":");
    address = splt[0];
    port = int.Parse(splt[1]);
}

var before = DateTime.Now;
var cl = new SourceClient(address, port);
Console.WriteLine("Connected.");
var time = (DateTime.Now - before).TotalMilliseconds;
Console.WriteLine("UDP Ping: " + time);

var info = cl.GetServerInfo();
foreach (var field in typeof(SourceInfo).GetFields())
    Console.WriteLine($"{field.Name} : {field.GetValue(info)}");

var players = cl.GetPlayers();
foreach (var ply in players)
{
    Console.WriteLine("===============");
    foreach (var field in typeof(Player).GetFields())
        Console.WriteLine($"{field.Name} : {field.GetValue(ply)}");
}